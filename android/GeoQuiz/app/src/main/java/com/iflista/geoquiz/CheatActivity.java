package com.iflista.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {

    public static final String EXTRA_ANSWER_IS_TRUE = "com.iflista.android.geoquiz.answer_is_true";
    public static final String EXTRA_ANSWER_SHOWN = "com.iflista.android.geoquiz.answer_shown";
    private static final String VERSION = "API level " + Integer.toString(Build.VERSION.SDK_INT);

    private boolean mAnswerIsTrue;

    private TextView mAnswerTextView;
    private TextView mApiVersionTextView;
    private Button mShowAnswer;

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);

        mAnswerTextView = (TextView) findViewById(R.id.answerTextView);

        mApiVersionTextView = (TextView)findViewById(R.id.api_version);
        mApiVersionTextView.setText(VERSION);
        // Answer will be shown only if user presses button
        if (savedInstanceState == null) {
            setAnswerShownResult(false);
        }

        mShowAnswer = (Button)findViewById(R.id.showAnswerButton);
        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mAnswerIsTrue) {
                    mAnswerTextView.setText(R.string.true_button);
                } else {
                    mAnswerTextView.setText(R.string.false_button);
                }
                setAnswerShownResult(true);
            }
        });
    }
}