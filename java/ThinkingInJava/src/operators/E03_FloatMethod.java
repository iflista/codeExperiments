package operators;

class Letter {
    float f;
}
public class E03_FloatMethod {
    static void f(Letter y) {
        y.f = 4.45f;
    }
    public static void main(String[] args) {
        Letter l = new Letter();
        l.f = 2.4f;
        System.out.println("l.f = " + l.f);
        f(l);
        System.out.println("l.f = " + l.f);
    }
}
