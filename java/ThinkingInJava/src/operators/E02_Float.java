package operators;

public class E02_Float {
    public static void main(String[] args) {
        Float f = new Float();
        f.f = 2.23f;
        Float fl = new Float();
        fl.f = 3.33f;
        System.out.println(f.f);
        System.out.println(fl.f);
        f.f = fl.f;
        System.out.println(f.f);
        System.out.println(fl.f);
    }
}
class Float {
    float f;
}