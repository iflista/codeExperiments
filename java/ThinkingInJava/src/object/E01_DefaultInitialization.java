package object;

public class E01_DefaultInitialization {
    int i;
    char c;
    public E01_DefaultInitialization() {
        System.out.println("int = " + i);
        System.out.println("char = [" + c + ']');
    }
    public static void main(String[] args) {
        new E01_DefaultInitialization();
    }
}
