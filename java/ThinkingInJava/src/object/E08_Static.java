package object;

public class E08_Static {
    static int i = 47;
    public static void main(String[] args) {
        E08_Static st1 = new E08_Static();
        E08_Static st2 = new E08_Static();
        System.out.println(st1.i + " == " + st2.i);
        st1.i++;
        System.out.println(st1.i + " == " + st2.i);
    }
}
