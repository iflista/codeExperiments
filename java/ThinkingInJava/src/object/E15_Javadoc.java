package object;

/** A first example from <i>s</i>.
 * Demonstrates the basic class
 * structure and the creation of a
 * <code>main()</code> method.
 */
public class E15_Javadoc {
    /** The <code>main()</code> method which is
     * called when the program is executed by saying
     * <code>java E15_HelloWorld</code>.
     * @param args array passed from the command-line
     */
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
