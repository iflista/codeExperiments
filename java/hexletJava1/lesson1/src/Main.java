import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Student studentMax = new Student();
        studentMax.course.name = "Java course";
        studentMax.sayHello();

        Student studentSlava = new Student();
        studentSlava.sayHello();
    }
}