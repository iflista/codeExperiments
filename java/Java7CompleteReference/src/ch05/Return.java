package ch05;

public class Return {
    public static void main(String[] args) {
        boolean t = true;

        System.out.println("Before executing return.");

        if (t) return;
        System.out.println("This operator will not execute.");
    }
}
