package ch07.CallByValue;

public class Test {
    void meth(int i, int j) {
        i *= 2;
        j /= 2;
    }
}
