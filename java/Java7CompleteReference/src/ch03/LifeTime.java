package ch03;

public class LifeTime {
    public static void main(String[] args) {
        int x;

        for (x=0; x<3; x++) {
            int y = -1; // y initializing every time for loop runs
            System.out.println("y equals: " + y); // this line always prints -1 value

            y = 100;
            System.out.println("y now equals: " + y);
        }
    }
}
