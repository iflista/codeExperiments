package ch03;

public class ScopeErr {
    public static void main(String[] args) {
        int bar = 1;
        { // creating new scope
         //   int bar = 2; // compiling error. varialble bar is already declared/
        }
    }
}
