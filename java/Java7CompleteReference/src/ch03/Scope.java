package ch03;

public class Scope {
    public static void main(String[] args) {
        int x; // this variable known to all code inside main method

        x = 10;
        if(x==10) { // start of new visibility scope
            int y = 20; // known only in this scope

            // x and y known in this scope
            System.out.println("x and y: " + x + " " + y);
            x = y * 2;
        }
        // y = 100; // Error! y is unknown in this scope

        //variable x is known here too
        System.out.println("x equals " + x);
    }
}
