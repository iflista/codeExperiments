package ch03;

// symbolic variables behave like integer variables
public class CharDemo2 {
    public static void main(String[] args) {
        char ch1;

        ch1 = 'X';
        System.out.println("ch1 contains " + ch1);

        ch1++; // increasing ch1 value by 1
        System.out.println("ch1 is now " + ch1);
    }
}
