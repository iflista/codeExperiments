package ch03;

// Calculating the distance, light will go
// using "long" type of data.
public class Light {
    public static void main(String[] args) {
        int lightspeed;
        long days;
        long seconds;
        long distance;

        // Speed of light in miles per second
        lightspeed = 186000;

        days = 1000; // number of days

        seconds = days * 24 * 60 * 60; // moving days into seconds

        distance = lightspeed * seconds; // Calculating distance

        System.out.print("In " + days);
        System.out.print(" days light will go ");
        System.out.println(distance + " miles.");
    }
}
