package ch03;

// Calculating area of a circle
public class Area {
    public static void main(String[] args) {
        double pi, r , a;

        r = 10.8; // radius of a circle
        pi = 3.1416; // value of pi
        a = pi * r * r; // calculating area

        System.out.println("Circle area is " + a);
    }
}
