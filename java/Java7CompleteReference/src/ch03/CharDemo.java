package ch03;

// Demonstration of char usage.
public class CharDemo {
    public static void main(String[] args) {
        char ch1, ch2;

        ch1 = 88; // variable X code
        ch2 = 'Y';

        System.out.print("ch1 and ch2: ");
        System.out.println(ch1 + " " + ch2);
    }
}
