package ch03;

public class BoolTest {
    public static void main(String[] args) {
        boolean b;

        b = false;
        System.out.println("b equals " + b);
        b = true;
        System.out.println("b equals " + b);

        // boolean value can control if
        if(b) System.out.println("This executes");
        b = false;
        if(b) System.out.println("This is not executes");

        // Result of boolean value comparison
        System.out.println("10 > 9 equals " + (10 > 9));
    }
}
