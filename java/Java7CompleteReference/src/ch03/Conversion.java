package ch03;

public class Conversion {
    public static void main(String[] args) {
        byte b;
        int i = 257;
        double d = 323.142;

        System.out.println("\nChanging int into byte.");
        b = (byte) i;
        System.out.println("i and b " + i + " " + b);

        System.out.println("\nChanging double into int.");
        i = (int) d;
        System.out.println("d and i " + d + " " + i);

        System.out.println("\nChanging double into byte.");
        b = (byte) d;
        System.out.println("d and b " + d + " " + b);
    }
}
