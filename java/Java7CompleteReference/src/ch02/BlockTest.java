package ch02;

/*
    Shows using code block.

    Call this file "ch02.BlockTest.java".
 */
class BlockTest {
    public static void main(String[] args) {
        int x, y;

        y = 20;

        // goal module of this cycle - block
        for (x=0; x<10; x++) {
            System.out.println("Value of x: " + x);
            System.out.println("Value of y: " + y);
            y=y-2;
        }
    }
}