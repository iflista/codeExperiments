package ch02;/*
    This is a simple Java program.
    please name this file "ch02.Example.java"
 */

class Example {
    // Program starts from calling main().
    public static void main(String[] args) {
        System.out.println("Simple Java program.");
    }
}