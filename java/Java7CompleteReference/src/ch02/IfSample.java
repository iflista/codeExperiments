package ch02;

/*
    Shows usage of if operator.

    Call this file "ch02.IfSample.java".
 */
class IfSample {
    public static void main(String[] args) {
        int x, y;

        x = 10;
        y = 20;

        if(x < y) System.out.println("x is less then y");

        x = x * 2;
        if(x == y) System.out.println("x is now equals y");

        x = x * 2;
        if(x > y) System.out.println("x now is larger than y");

        // This operator will not show anything
        if(x == y) System.out.println("you will not see this");
    }
}