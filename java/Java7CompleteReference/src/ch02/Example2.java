package ch02;/*
    This is another short example.
    Name this file "ch02.Example2.java".
 */

class Example2 {
    public static void main(String[] args) {
        int num; // this line declares variable with name num

        num = 100; // this line adds value 100 to num variable

        System.out.println("This is a num variable " + num);

        num = num + 2;

        System.out.print("Value of num varialbe * 2 equals ");
        System.out.println(num);
    }
}